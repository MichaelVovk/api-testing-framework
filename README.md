# API Testing Framework

[![GitlabCI](https://gitlab.com/MichaelVovk/api-testing-framework/badges/master/pipeline.svg?style=shield)](https://gitlab.com/MichaelVovk/api-testing-framework/-/pipelines/latest)

## Technical stack

* Java : [Java 8](https://jdk.java.net/8/)
* Build : [Maven](https://maven.apache.org/)
* Testing : [JUnit](https://junit.org/junit5/)
* Framework : [Serenity](https://serenity-bdd.github.io)
* DBB : [Cucumber/Gherkin](  https://cucumber.io/)

## How to run

mvn clean verify

## Reports

* Results/Documents: target/site/serenity


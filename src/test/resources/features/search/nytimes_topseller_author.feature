Feature: The Books API information can be filtered bu author

  Scenario Outline: Filter by the author of the best seller
    Given Author name '<author>'
    When Get The best sellers list by author
    Then All Books in the should contain '<author>'
    Examples:
      | author              |
      | Diana Gabaldon      |
      | Gary Vaynerchuk     |
      | Annette Gordon-Reed |

  Scenario Outline: Filter by Not existing author of the best seller
    Given Not existing author name '<author>'
    When Get The best sellers list by author
    Then Results are empty
    Examples:
      | author |
      | null |
      | 漢字  |
      | !@#$%^&*)( |


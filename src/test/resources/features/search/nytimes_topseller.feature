Feature: The Books API is secured using API Key

  Scenario: Get Best Sellers list history
    Given Valid API key
    When Get The best sellers list
    Then Response should contain copyright 'Copyright (c) 2021 The New York Times Company.  All Rights Reserved.'
    And Response should contain num_results


  Scenario: Get Best Sellers list history with invalid api Key
    Given Invalid API key 'YcGz5G9NA8OEiyLF7GmWb9RpANvnTy22'
    When Get The best sellers list with invalid api Key
    Then Unauthorized response 'Invalid ApiKey'


  Scenario: Get Best Sellers list history with no api Key
    Given No API key
    When Get The best sellers list with no api key
    Then Unauthorized response 'Failed to resolve API Key variable request.queryparam.api-key'




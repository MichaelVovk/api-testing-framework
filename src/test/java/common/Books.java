package common;

import models.BestSellersListPOJO;
import net.serenitybdd.rest.SerenityRest;

public class Books {

    public BestSellersListPOJO returned() {
        return SerenityRest.lastResponse().getBody().as(BestSellersListPOJO.class);
    }
}

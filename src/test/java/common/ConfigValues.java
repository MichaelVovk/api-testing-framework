package common;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import static java.util.stream.Collectors.toMap;

public class ConfigValues {
    public static Map<String, String> in(String propertiesFile)  {
        Properties properties = new Properties();
        try {
            properties.load(ConfigValues.class.getResourceAsStream("/" + propertiesFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.entrySet()
                .stream()
                .collect(toMap(entry -> entry.getKey().toString(),
                               entry -> entry.getValue().toString()));
    }
}

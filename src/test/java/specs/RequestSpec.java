package specs;


import common.ConfigValues;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class RequestSpec {

    private RequestSpec(){}

    public static RequestSpecification get() {

        return  new RequestSpecBuilder()
                .setBaseUri(ConfigValues.in("config.properties").get("api.endpoint"))
                .setContentType(ContentType.JSON)
                .addFilter(new ResponseLoggingFilter())
                .addFilter(new RequestLoggingFilter())
                .addQueryParam("api-key",ConfigValues.in("config.properties").get("api.key"))
                .build();
    }

    public static RequestSpecification getNoKey() {

        return  new RequestSpecBuilder()
                .setBaseUri(ConfigValues.in("config.properties").get("api.endpoint"))
                .setContentType(ContentType.JSON)
                .addFilter(new ResponseLoggingFilter())
                .addFilter(new RequestLoggingFilter())
                .build();
    }
}

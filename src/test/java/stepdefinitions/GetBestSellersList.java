package stepdefinitions;

import io.cucumber.java.Before;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Step;
import specs.RequestSpec;


public class GetBestSellersList {
    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Step("get best sellers list by author")
    public void byAuthor(String author) {
        SerenityRest.given()
                .spec(RequestSpec.get())
                .queryParam("author", author)
                .when()
                .get();

    }

    @Step("get best sellers list")
    public void get() {
        SerenityRest.given()
                .spec(RequestSpec.get())
                .when()
                .get();

    }


    @Step("get best sellers list with custom")
    public void getWithApiKey(String apiKey) {
        SerenityRest.given()
                .spec(RequestSpec.getNoKey())
                .queryParam("api-key", apiKey)
                .when()
                .get();

    }

    @Step("get best sellers list without apiKey")
    public void getWithOutKey() {
        SerenityRest.given()
                .spec(RequestSpec.getNoKey())
                .when()
                .get();

    }
}

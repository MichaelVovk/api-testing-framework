package steps;

import common.Books;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import specs.ResponseSpec;
import stepdefinitions.GetBestSellersList;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;

public class GetBestSellersHistory {

    @Steps
    GetBestSellersList getBestSellersList;

    @Steps
    Books books;

    String apiKey;

    @Given("Valid API key")
    public void valid_api_key() {
    }

    @When("Get The best sellers list")
    public void get_the_best_sellers_list() {
        getBestSellersList.get();
    }

    @Then("Response should contain copyright {string}")
    public void response_should_contain_copyright(String copyright) {

        restAssuredThat(response -> response.spec(ResponseSpec.okResponse()));
        assertThat(books.returned().getCopyright()).contains(copyright);

    }

    @Then("Response should contain num_results")
    public void response_should_contain_num_results() {

        restAssuredThat(response -> response.spec(ResponseSpec.okResponse()));
        assertThat(books.returned().getResults()).isNotNull().isNotEmpty();

    }

    @Given("Invalid API key {string}")
    public void invalid_api_key(String apiKey) {
        this.apiKey = apiKey;
    }

    @When("Get The best sellers list with invalid api Key")
    public void get_The_best_sellers_list_with_invalid_api_Key() {

        getBestSellersList.getWithApiKey(apiKey);
    }

    @Then("Unauthorized response {string}")
    public void unauthorized_response(String faultstring) {

        restAssuredThat(response -> response.spec(ResponseSpec.unauthorized(faultstring)));

    }

    @Given("No API key")
    public void no_api_key() {
    }

    @When("Get The best sellers list with no api key")
    public void get_The_best_sellers_list_with_no_api_key() {
        getBestSellersList.getWithOutKey();
    }

}

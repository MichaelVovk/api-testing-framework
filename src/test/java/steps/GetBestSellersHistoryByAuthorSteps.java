package steps;

import common.Books;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import specs.ResponseSpec;
import stepdefinitions.GetBestSellersList;


import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;

public class GetBestSellersHistoryByAuthorSteps {

    @Steps
    GetBestSellersList getBestSellersList;

    @Steps
    Books books;

    String author;

    @Given("Author name {string}")
    public void author_name(String author) {
        this.author = author;
    }

    @When("Get The best sellers list by author")
    public void get_The_best_sellers_list() {
        getBestSellersList.byAuthor(author);
    }

    @Then("All Books in the should contain {string}")
    public void all_Books_with_this_author(String author) {

        restAssuredThat(response -> response.spec(ResponseSpec.okResponse()));

        books.returned().getResults().forEach(s -> assertThat(s.getAuthor()).contains(author));

    }

    @Given("Not existing author name {string}")
    public void invalid_author_name(String author) {
        this.author = author;
    }

    @Then("Results are empty")
    public void notFound_books_with_this_author() {

        restAssuredThat(response -> response.spec(ResponseSpec.okResponse()));

        assertThat(books.returned().getResults()).asList().isEmpty();

        assertThat(books.returned().getNumResults()).isZero();

    }

}
